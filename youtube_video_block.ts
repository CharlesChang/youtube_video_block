declare const firebase: any;
(function () {
    'use strict';
    'allow pasting';



    // Initialize Firebase
    const config = {
        apiKey: "AIzaSyDdWPrhGe2C751y0JJavb3hKD-djtbNk0U",
        authDomain: "youtube-block.firebaseapp.com",
        databaseURL: "https://youtube-block.firebaseio.com",
        storageBucket: "youtube-block.appspot.com",
        messagingSenderId: "719277716109"
    };
    firebase.initializeApp(config);

    const firebaseRoot = firebase.database().ref();

    const debug: boolean = true;
    let BanishDuration: number = 7;
    let currentURL: string = "";
    let dataChanged: boolean = true;
    let updateInterval: number = 2000;
    let localList = {};
    //=============================================  HTML  ============================================


    // button sets
    document.querySelector("#yt-masthead-user").insertAdjacentHTML(`beforeend`,
        `
        <button id="addtoBL" type="button" class="GM_button" > 7 </button>
        <button id="settings" class="GM_button" type="button" > Settings </button>
        <button id="addtoBL30" class="GM_button" type="button" > 30 </button>
        `
    )

    // dropdown content
    document.querySelector("#masthead-positioner").insertAdjacentHTML(`beforeend`,
        `
         <div id="dropdownPanel">
            <div class="wrapper">
                
                <p style="display:inline-block">Duration: <span id="duration">TBD</span> Days</p>
                <br>                
                <form >
                    <label >Change Duration?</label>
                    <button  class="setDuration" value="7" >7</button> 
                    <button class="setDuration" value="30" >30</button> 
                    <button  class="setDuration" value="infinity">Infinity</button> 
                </form>
                <br>
                
                <div id="prisonCells"></div>
            </div>
        <div>
        `
    );


    document.querySelector('#addtoBL').addEventListener(`click`, function () {
        banishSong();
    });
    document.querySelector('#addtoBL30').addEventListener(`click`, function () {
        banishSong(30);
    });

    document.querySelector('#settings').addEventListener(`click`, function () {
        document.getElementById("dropdownPanel").classList.toggle("show");
        renderList();
    });

    Array.from(document.querySelectorAll('#setDuration')).forEach(val => val.addEventListener(`click`, () => {
        BanishDuration = parseFloat(val.getAttribute(`value`));
    }))
    //=============================================  CSS ===============================================






    // ======================================== JavsScripts  ===============================================


    // update function
    setInterval(function () {

        // 1. check url change
        if (window.location.href != currentURL) {
            checkBlacklist();
            currentURL = window.location.href;

        }

    }, updateInterval);

    firebase.database.enableLogging(true);

    let blacklistRef = firebase.database().ref("/blacklist");
    blacklistRef.on("value", function (dataSnapshot) {
        console.log("fetching list from server");
        let response = dataSnapshot.val();
        for (let pushkey in response) {
            if (!response.hasOwnProperty(pushkey)) continue;

            if (response[pushkey].duration !== "perm" && (Date.now() - response[pushkey].timeCreated) / 8.64e+7 > parseInt(response[pushkey].duration)) {
                // discard expired item
                var dataRef = firebase.database().ref(`blacklist/${pushkey}`);
                dataRef.remove()
                    .then(() => console.log("expired song removed"))
                    .catch(error => console.log("Remove failed: " + error.message));
            } else {
                // store valid item
                localList[response[pushkey].videoId] = {
                    pushkey: pushkey,
                    title: response[pushkey].title,
                    duration: response[pushkey].duration
                };
            }

        }
    }, (err) => {
        console.log(err);
    });

    blacklistRef.on('child_changed', res => {
        console.log("updating list from the server");
        dataChanged = true;
        let response = res.val();
        for (let pushkey in response) {
            if (response.hasOwnProperty(pushkey)) {

                localList[response[pushkey].videoId] = {
                    pushkey: pushkey,
                    title: response[pushkey].title,
                    duration: response[pushkey].duration

                }
            }
        }
    }, err => console.log(err));

    function checkBlacklist() {
        // getCookieBlacklist();
        for (var key in localList) {
            if (localList.hasOwnProperty(key)) {
                if (window.location.href.indexOf(key) > -1) {
                    if (debug) console.log("blacklisted video found");
                    nextLink();
                    break;
                }
            }
        }
    }

    //when clicked with button add to black list and jump to next song
    function banishSong(duration?: number) {
        if (document.querySelectorAll(".autoplay-bar").length) {
            var url = window.location.href;
            var indexOfId_start = url.indexOf("=") + 1;
            var videoId = url.substring(indexOfId_start, indexOfId_start + 11);
            var title = document.getElementById("eow-title").title;

            var pushid = blacklistRef.push({
                videoId: videoId,
                title: title,

                duration: duration ? duration : BanishDuration,
                timeCreated: Date.now()
            })
            if (!pushid) {
                alert("pushing failed")
            }
            nextLink();
        } else {
            alert("It seems you are not at the correct page to perform this action");
        }
    }

    function nextLink() {
        //select next video button
        var nextUrl = document.getElementsByClassName("autoplay-bar")[0].getElementsByTagName("a")[0].href;
        if (nextUrl) {
            window.location.href = nextUrl;
        } else {
            alert("Fail to jump to next video because next URl isn't found");
        }
    }

    function freeSong(id) {
        let cfm = confirm("Confirm delete?");
        if (cfm) {
            var adaRef = firebase.database().ref(`blacklist/${localList[id].pushkey}`);
            adaRef.remove()
                .then(function () {
                    console.log("Remove succeeded.")
                })
                .catch(function (error) {
                    console.log("Remove failed: " + error.message)
                });
        }
    }

    function renderList() {
        if (dataChanged) { //only update the list if there are changes in the cookies
            var count = 1;
            var $prisonCells = document.querySelector("#prisonCells");
            if (localList == {}) {
                $prisonCells.textContent = " No demon captured ";
            } else {
                $prisonCells.textContent = ""; //clear content first
                $prisonCells.innerHTML = `<h3 style="text-align:center;"> Demon Captured</h3>`;
                for (var key in localList) {
                    if (localList.hasOwnProperty(key)) { // for each demon, create a tr to hold its data
                        $prisonCells.insertAdjacentHTML('beforeend',
                            `
                            <div class="cell">
                                <p>${count}. ${localList[key].title}</p>
                                <button id="key${key}">Delete</button>
                            </div>
                        `
                        );


                        count++;
                        // initiate an event handle for click``
                        document.querySelector(`#key${key}`).addEventListener(`click`, function () {
                            freeSong(key);
                            document.querySelector("#key${key}").parentElement.remove();
                        });
                    }
                }
            }
            dataChanged = false;
        }
    }
})(); //The end of the code
